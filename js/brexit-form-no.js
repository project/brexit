(function (Drupal, drupalSettings, $) {

  Drupal.behaviors.formNo = {
    attach: function attach(context) {
      var $noButton = $('<input type="button" id="edit-submit-no" name="no" value="' + Drupal.t('No.') + '" class="button js-form-submit form-submit">').on('mousedown keydown', function(e) {
        $(this).clone().insertAfter(this);
        e.preventDefault();
      });
      $('[data-drupal-selector=edit-actions]', context).append($noButton);
    }
  };

})(Drupal, drupalSettings, jQuery);
