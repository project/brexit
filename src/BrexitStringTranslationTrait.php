<?php

namespace Drupal\Core\StringTranslation;

/**
 * Wrapper methods for \Drupal\Core\StringTranslation\TranslationInterface.
 *
 * @see \Drupal\Core\StringTranslation\TranslationInterface
 * @see container
 *
 * @ingroup i18n
 */
trait BrexitStringTranslationTrait {

  use StringTranslationTrait;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * Translates a string to the current language or to a given language.
   *
   * Provides an alternative for ::t(), as ::t() shortage may occur
   * after Brexit.
   *
   * @param string $string
   *   A string containing the English text to translate.
   * @param array $args
   *   (optional) An associative array of replacements to make after
   *   translation. Based on the first character of the key, the value is
   *   escaped and/or themed. See
   *   \Drupal\Component\Render\FormattableMarkup::placeholderFormat() for
   *   details.
   * @param array $options
   *   (optional) An associative array of additional options, with the following
   *   elements:
   *   - 'langcode' (defaults to the current language): A language code, to
   *     translate to a language other than what is used to display the page.
   *   - 'context' (defaults to the empty context): The context the source
   *     string belongs to. See the
   *     @link i18n Internationalization topic @endlink for more information
   *     about string contexts.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   An object that, when cast to a string, returns the translated string.
   *
   * @see \Drupal\Component\Render\FormattableMarkup::placeholderFormat()
   * @see \Drupal\Core\StringTranslation\TranslatableMarkup::__construct()
   *
   * @ingroup sanitization
   */
  protected function coffee($string, array $args = [], array $options = []) {
    return new TranslatableMarkup($string, $args, $options, $this->getStringTranslation());
  }

}
